import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ControleurMorpion implements MouseListener {
	private VueMorpion vue;
	private ModeleMorpion modele;

	public ControleurMorpion(VueMorpion vue, int currentPlayer) {
		this.vue = vue;
		this.modele = new ModeleMorpion();
	}

	public void reset() {
		this.modele.initialiser();
	}

	@Override
	public void mouseClicked(MouseEvent clickEvent) {
		if(clickEvent.getComponent() instanceof JButtonMorpion) {
			JButtonMorpion btn = (JButtonMorpion) clickEvent.getSource();
			if(btn.getText().equals("")) {
				Couple c = this.vue.coordonneesBtCaseGrille(btn);
				if(btn.isEnabled()) {
					this.modele.setValeurCase(c.getPremier(), c.getSecond(), this.modele.getCurrentPlayer() == 1 ? TypeCase.JOUEUR1 : TypeCase.JOUEUR2);
					btn.setText(this.modele.getCurrentPlayer() == 1 ? "X" : "O");
					btn.setEnabled(false);
				}

				if(this.modele.isDraw() || this.modele.casesAlignees(c.getPremier(), c.getSecond())) {
					if(this.modele.isDraw()) {
						this.vue.drawGame();
					} else {
						this.vue.afficherResultat(this.modele.getCurrentPlayer());
					}
					this.vue.disableButtons();
				} 
				
				if(btn.isEnabled()) {
					this.modele.switchPlayer();
					vue.afficherJoueurCourant(this.modele.getCurrentPlayer());
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
}
