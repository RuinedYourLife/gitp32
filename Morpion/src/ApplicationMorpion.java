import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class ApplicationMorpion {
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setTitle("Morpion");
		f.setSize(500, 300);
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(0);
			}
		});
		f.add(new VueMorpion());
		f.setVisible(true);
	}
}
