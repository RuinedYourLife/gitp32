
public class ModeleMorpion {
	public static final int TAILLE_GRILLE = 3;
	private TypeCase[][] cases = new TypeCase[TAILLE_GRILLE][TAILLE_GRILLE];
	private int currentPlayer;
	
	public ModeleMorpion() {
		this.initialiser();
	}
	
	public void initialiser() {
		this.currentPlayer = 1;
		for(int y = 0; y < this.cases.length; y++) {
			for(int x = 0; x < this.cases[y].length; x++) {
				this.cases[y][x] = TypeCase.VIDE;
			}
		}
	}
	
	public void switchPlayer() {
		this.currentPlayer = this.currentPlayer == 1 ? 2 : 1;
	}
	
	public int getCurrentPlayer() {
		return this.currentPlayer;
	}
	
	public TypeCase getValeurCase(int x, int y) {
		return this.cases[y][x];
	}
	
	public void setValeurCase(int x, int y, TypeCase type) {
		this.cases[y][x] = type;
	}
	
	public boolean isDraw() {
		for(int y = 0; y < this.cases.length; y++) {
			for(int x = 0; x < this.cases[y].length; x++) {
				if(this.cases[y][x] == TypeCase.VIDE) return false;
			}
		}
		return true;
	}
	
	public boolean ligneFinie(int y) {
		return this.getValeurCase(0, y) != TypeCase.VIDE && this.getValeurCase(0, y) == this.getValeurCase(1, y) && this.getValeurCase(1, y) == this.getValeurCase(2, y);
	}
	
	public boolean colonneFinie(int x) {
		return this.getValeurCase(x, 0) != TypeCase.VIDE && this.getValeurCase(x, 0) == this.getValeurCase(x, 1) && this.getValeurCase(x, 1) == this.getValeurCase(x, 2);
	}
	
	public boolean premiereDiagonaleFinie() {
		return this.getValeurCase(0, 0) != TypeCase.VIDE && this.getValeurCase(0, 0) == this.getValeurCase(1, 1) && this.getValeurCase(1, 1) == this.getValeurCase(2, 2);
	}
	
	public boolean deuxiemeDiagonaleFinie() {
		return this.getValeurCase(2, 0) != TypeCase.VIDE && this.getValeurCase(2, 0) == this.getValeurCase(1, 1) && this.getValeurCase(1, 1) == this.getValeurCase(0, 2);
	}

	public boolean casesAlignees(int x, int y) {
		return ligneFinie(y) || colonneFinie(x) || premiereDiagonaleFinie() || deuxiemeDiagonaleFinie();
	}
}
