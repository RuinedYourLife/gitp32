import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ResetButtonListener implements MouseListener {	
	private VueMorpion vueMorpion;
	
	public ResetButtonListener(VueMorpion vueMorpion) {
		this.vueMorpion = vueMorpion;
	}
	public void mouseClicked(MouseEvent arg0) {
		vueMorpion.initialiser();
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
