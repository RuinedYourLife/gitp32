import javax.swing.JFrame;

import memory.view.VueMemory;

public class MemoryWindow extends JFrame {
	private static final long serialVersionUID = 2954989570240676627L;

	public MemoryWindow() {
		this.setSize(600, 600);
		this.setTitle("Memoji");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(new VueMemory());
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new MemoryWindow();
	}
}
