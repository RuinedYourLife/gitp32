package memory.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ModelMemory {
	public static int GRID_SIZE = 4;
	
	private int playerTries;
	private int correctGuesses;
	private long startTime;
	private Card[][] grid;
	
	public ModelMemory() {
		this.playerTries = 0;
		this.correctGuesses = 0;
		this.grid = new Card[GRID_SIZE][GRID_SIZE];
		this.startTime = System.currentTimeMillis();
	}
	
	public List<String> getIconsList() {
		List<String> icons = new ArrayList<>();
		
		File currentDirectory = new File("./src/assets/img/");
		for(String filename : currentDirectory.list()) {
			if(filename.equals("hidden.png")) continue;
			icons.add(filename);
		}
		
		return icons;
	}
	
	public int getPlayerTries() {
		return this.playerTries;
	}
	
	public int getCorrectGuesses() {
		return this.correctGuesses;
	}
	
	public long getStartTime() {
		return this.startTime;
	}
	
	public Card[][] getGrid() {
		return this.grid;
	}
	
	public List<String> getRandomIcons() {
		List<String> icons = new ArrayList<>();
		List<String> availableIcons = this.getIconsList();
		
		Collections.shuffle(availableIcons);
		for(int i = 0; i < (GRID_SIZE * GRID_SIZE) / 2; i++) {
			for(int j = 0; j < 2; j++) {
				icons.add(availableIcons.get(i));
			}
		}

		Collections.shuffle(icons);
		return icons;
	}
	
	public void initialize() {
		this.playerTries = 0;
		this.correctGuesses = 0;
		this.startTime = System.currentTimeMillis();
		this.initializeGrid();
	}
	
	private void initializeGrid() {
		List<String> icons = this.getRandomIcons();
		for(int y = 0; y < GRID_SIZE; y++) {
			for(int x = 0; x < GRID_SIZE; x++) {
				this.grid[y][x] = new Card(icons.get(y * GRID_SIZE + x));
			}
		}
	}
	
	public void play(boolean correct) {
		this.playerTries++;
		if(correct) this.correctGuesses++;
	}
	
	public boolean isGameFinished() {
		for(int y = 0; y < GRID_SIZE; y++) {
			for(int x = 0; x < GRID_SIZE; x++) {
				if(this.grid[y][x].getState() != CardState.VISIBLE) {
					return false;
				}
			}
		}
		
		return true;
	}
}
