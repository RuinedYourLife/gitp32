package memory.model;

public class Card {
	private String icon;
	private CardState state;
	
	public Card(String icon) {
		this.icon = icon;
		this.state = CardState.HIDDEN;
	}
	
	public void reveal() {
		this.state = CardState.VISIBLE;
	}
	
	public void hide() {
		this.state = CardState.HIDDEN;
	}
	
	public String getIcon() {
		return this.state == CardState.VISIBLE ? this.icon : "hidden.png";
	}
	
	public CardState getState() {
		return this.state;
	}
	
	public boolean equals(Card other) {
		return this.getIcon().equals(other.getIcon());
	}
}
