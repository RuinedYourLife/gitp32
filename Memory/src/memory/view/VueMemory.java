package memory.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import memory.controller.ControllerMemory;
import memory.model.Card;
import memory.model.ModelMemory;

public class VueMemory extends JPanel {
	private static final long serialVersionUID = -511812802901826596L;
	
	private CardButton[][] cardsGrid;
	private ControllerMemory controller;
	
	private JLabel guessesLabel;
	private JLabel timer;
	
	private JButton replay;
	
	public VueMemory() {
		this.controller = new ControllerMemory(this);
		this.setLayout(new BorderLayout());

		JPanel container = new JPanel(new BorderLayout());

		this.cardsGrid = new CardButton[ModelMemory.GRID_SIZE][ModelMemory.GRID_SIZE];
		JPanel gridPanel = new JPanel(new GridLayout(0, 4));

		for(int y = 0; y < this.cardsGrid.length; y++) {
			for(int x = 0; x < this.cardsGrid[y].length; x++) {
				this.cardsGrid[y][x] = new CardButton(x, y);
				this.cardsGrid[y][x].addActionListener(this.controller);
				this.cardsGrid[y][x].setActionCommand("play");
				gridPanel.add(this.cardsGrid[y][x]);
			}
		}

		JPanel southBar = new JPanel(new GridLayout(1, 3));
		
		JPanel southLeft = new JPanel();
		southLeft.add(new JLabel("Essais: "));
		this.guessesLabel = new JLabel("0 / 0", SwingConstants.CENTER);
		southLeft.add(this.guessesLabel);
		
		southBar.add(southLeft);
		
		this.replay = new JButton("Rejouer");
		this.replay.setActionCommand("replay");
		this.replay.addActionListener(this.controller);
		southBar.add(replay);
		
		this.timer = new JLabel("00:00:00", SwingConstants.CENTER);
		southBar.add(this.timer);

		
		
		container.add(gridPanel, BorderLayout.CENTER);
		container.add(southBar, BorderLayout.SOUTH);

		this.add(container, BorderLayout.CENTER);
		
		this.controller.initialize();
	}
	
	public void updateGrid(Card[][] grid) {
		for(int y = 0; y < grid.length; y++) {
			for(int x = 0; x < grid[y].length; x++) {
				CardButton tmpButton = this.cardsGrid[y][x];
				tmpButton.setIconFilename(grid[y][x].getIcon());
			}
		}
	}
	
	public void displayReplayButton(boolean display) {
		this.replay.setVisible(display);
	}
	
	public void updateInformations(int playerTries, int correctGuesses) {
		this.guessesLabel.setText(correctGuesses + " / " + playerTries);
	}
	
	public void updateTimer(int seconds) {
		this.timer.setText(formatSeconds(seconds));
	}
	
	private String formatSeconds(int seconds) {
		int s = seconds % 60;
		int m = (seconds / 60) % 60;
		int h = (seconds / 3600) % 24;
		return String.format("%02d:%02d:%02d", h, m , s);
	}
}
