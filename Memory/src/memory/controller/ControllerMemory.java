package memory.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

import memory.model.Card;
import memory.model.CardState;
import memory.model.ModelMemory;
import memory.view.CardButton;
import memory.view.VueMemory;

public class ControllerMemory implements ActionListener {
	private VueMemory vue;
	private ModelMemory model;
	private GameState state;
	private Card clickedCard;

	private Timer timer;

	public ControllerMemory(VueMemory vue) {
		this.vue = vue;
		this.model = new ModelMemory();
	}

	public void initialize() {
		this.state = GameState.INITIAL_STATE;
		this.model.initialize();
		this.vue.updateGrid(this.model.getGrid());
		this.vue.updateInformations(0, 0);
		this.vue.updateTimer(0);
		this.vue.displayReplayButton(false);
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerRunnable(this.model.getStartTime(), this.vue), 0L, 1000L);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("play")) {
			if(e.getSource() instanceof CardButton) {
				CardButton cardButton = (CardButton) e.getSource();
				this.playCard(cardButton);
			}
		} else if(e.getActionCommand() == "replay") {
			this.replay();
		}
	}

	private void playCard(CardButton cardButton) {
		Card card = this.model.getGrid()[cardButton.getRow()][cardButton.getColumn()];
		if(card.getState() == CardState.VISIBLE) return;
		switch(this.state) {
		case INITIAL_STATE:
			card.reveal();
			this.clickedCard = card;
			this.state = GameState.ONE_CARD_CLICKED;
			break;
		case ONE_CARD_CLICKED:
			card.reveal();
			boolean correct = this.clickedCard.equals(card);
			this.state = GameState.TWO_CARDS_CLICKED;
			System.out.println((correct ? "Correct !" : "Nope") + " (" + this.clickedCard.getIcon() + " / " + card.getIcon() + ")");
			if(!correct) {
				new Timer("HideCardButton").schedule(new HideCardRunnable(this.vue, this, this.model.getGrid(), card, this.clickedCard), 1000L);
			} else {
				this.state = GameState.INITIAL_STATE;
			}
			this.model.play(correct);
			this.vue.updateInformations(this.model.getPlayerTries(), this.model.getCorrectGuesses());

			if(this.model.isGameFinished()) {
				this.state = GameState.FINISHED;
				System.out.println("Le jeu est fini !");
				this.timer.cancel();
				this.vue.displayReplayButton(true);
			}
		case TWO_CARDS_CLICKED:
			break;
		case FINISHED:
			System.out.println("Le jeu est fini !");
			break;
		}

		this.vue.updateGrid(this.model.getGrid());
	}
	
	private void replay() {
		this.initialize();
	}

	public GameState getState() {
		return this.state;
	}

	public void setState(GameState state) {
		this.state = state;
	}
}
