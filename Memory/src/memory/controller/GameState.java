package memory.controller;

public enum GameState {
	INITIAL_STATE, ONE_CARD_CLICKED, TWO_CARDS_CLICKED, FINISHED
}
