package memory.controller;

import java.util.TimerTask;

import memory.model.Card;
import memory.model.ModelMemory;
import memory.view.VueMemory;

public class HideCardRunnable extends TimerTask {
	private VueMemory vue;
	private ControllerMemory controller;
	private Card[][] grid;
	private Card firstCard;
	private Card secondCard;
	
	public HideCardRunnable(VueMemory vue, ControllerMemory controller, Card[][] grid, Card firstCard, Card secondCard) {
		this.vue = vue;
		this.controller = controller;
		this.grid = grid;
		this.firstCard = firstCard;
		this.secondCard = secondCard;
	}
	
	public void run() {
		this.firstCard.hide();
		this.secondCard.hide();
		this.vue.updateGrid(this.grid);
		this.controller.setState(GameState.INITIAL_STATE);
	}
}
