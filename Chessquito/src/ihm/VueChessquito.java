package ihm;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class VueChessquito extends JPanel {
	private static final long serialVersionUID = -6678894294485253358L;
	private JTextArea messages;
	private JPanel conteneurPlateau;
	private JPanel panneauPlateau;
	private CaseIHM[][] cases = new CaseIHM[4][4];
	private ControleurChessquito controleur;

	public VueChessquito() {
		this.setLayout(new BorderLayout());

		this.messages = new JTextArea(4, 40);
		this.messages.setEditable(false);

		this.conteneurPlateau = new JPanel(new BorderLayout());
		
		this.panneauPlateau = new JPanel();
		this.panneauPlateau.setLayout(new GridLayout(4, 4));

		this.controleur = new ControleurChessquito(this);
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				this.cases[i][j] = new CaseIHM(i, j, (i+j) % 2 == 0 ? Color.lightGray : Color.white);
				this.cases[i][j].addActionListener(this.controleur);
				this.panneauPlateau.add(this.cases[i][j]);
			}
		}

		this.controleur.rafraichir();

		CardLayout alternateur = new CardLayout();
		
		JPanel selectPanel = new JPanel(alternateur);
			
		String pieces[] = {"cavalier", "fou", "pion", "reine"};
		
		JPanel whiteSelect = new JPanel(new GridLayout(4, 1));
		for(String piece : pieces) {
			JButton btn = new JButton();
			btn.setIcon(new PieceIHM(piece, "Blanc", "Fixe"));
			whiteSelect.add(btn);
		}
		
		JPanel blackSelect = new JPanel(new GridLayout(4, 1));
		for(String piece : pieces) {
			JButton btn = new JButton();
			btn.setIcon(new PieceIHM(piece, "Blanc", "Fixe"));
			blackSelect.add(btn);
		}
		
		selectPanel.add("white", whiteSelect);
		selectPanel.add("black", blackSelect);
		
		selectPanel.setBorder(new EmptyBorder(0, 0, 0, 6));
		
		alternateur.show(selectPanel, "white");
		
		this.conteneurPlateau.add(selectPanel, BorderLayout.WEST);

		this.conteneurPlateau.add(this.panneauPlateau, BorderLayout.CENTER);
		
		this.add(this.conteneurPlateau, BorderLayout.CENTER);

		this.add(new JScrollPane(this.messages), BorderLayout.SOUTH);
		
		this.afficherMessage("Positionnez les pièces sur l'échiquier");
	}

	public void positionnerPiece(PieceIHM piece, int i, int j) {
		if(i < 0 || i > 3 || j < 0 || j > 3) throw new IllegalArgumentException("Out of bounds");
		CaseIHM casePiece = this.cases[i][j];
		casePiece.setRolloverEnabled(true);
		casePiece.setIcon(piece);
	}

	public void afficherMessage(String message) {
		this.messages.append(message);
		this.messages.setSelectionEnd(this.messages.getText().length());
	}
}
