import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

/**
 * controleur a l'ecoute des changements de focus sur les champs de texte
 * contenant les valeurs des nuances d'une couleur
 */
public class ControleurCouleur implements CaretListener {
	/**
	 * vue associee au controleur
	 */
	private VueCouleur vue;

	/**
	 * cree un controleur en lui associant sa vue
	 * 
	 * @param vue
	 *            : vue associee au controleur
	 */
	public ControleurCouleur(VueCouleur vue) {
		this.vue = vue;
	}

	/**
	 * si on perd le focus sur un champ de texte alors recalculer la nouvelle
	 * valeur RVB de la couleur en fonction des trois nuances RVB et afficher
	 * cette couleur dans le canevas cCouleur de la vue
	 * 
	 * @param e
	 *            evenement de focus
	 */

	@Override
	public void caretUpdate(CaretEvent arg0) {
		Color c = this.getColor();
		this.vue.setCouleurCanvas(c);
		this.vue.setHexColor(c);
	}
	
	public Color getColor() {
		int v1 = !this.vue.getTRouge().getText().isEmpty() ? Math.min(Integer.parseInt(this.vue.getTRouge().getText()), 255): 0;
		// convertir le contenu du deuxieme champ de texte
		// en un entier pour obtenir la nuance verte
		int v2 = !this.vue.getTVert().getText().isEmpty() ? Math.min(Integer.parseInt(this.vue.getTVert().getText()), 255) : 0;
		// convertir le contenu du troisieme champ de texte
		// en un entier pour obtenir la nuance bleue
		int v3 = !this.vue.getTBleu().getText().isEmpty() ? Math.min(Integer.parseInt(this.vue.getTBleu().getText()), 255) : 0;
		
		return new Color(v1, v2, v3);
	}
}
