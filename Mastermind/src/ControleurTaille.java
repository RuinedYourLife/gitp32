import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;

public class ControleurTaille extends ControleurFenetre {
	
	public ControleurTaille(FenetreMastermind fenetre) {
		super(fenetre);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			this.fenetre.changerItemTailleCombinaison((JMenuItem) e.getSource()); 
		}
	}
}
