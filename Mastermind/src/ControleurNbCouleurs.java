import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;

public class ControleurNbCouleurs extends ControleurFenetre {

	public ControleurNbCouleurs(FenetreMastermind fenetre) {
		super(fenetre);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			this.fenetre.changerItemNbCouleurs((JMenuItem) e.getSource());
		}
	}
}
