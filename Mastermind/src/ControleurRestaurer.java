import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class ControleurRestaurer extends ControleurFenetre implements ActionListener {
	
	public ControleurRestaurer(FenetreMastermind fenetre) {
		super(fenetre);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment restaurer le dernier jeu ?") == JOptionPane.YES_OPTION) {
			fenetre.restaurerVueMastermindFichier("sauvegarde.mastermind");
		}
	}

}
