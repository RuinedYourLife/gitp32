import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class ControleurSauvegarder extends ControleurFenetre implements ActionListener {

	public ControleurSauvegarder(FenetreMastermind fenetre) {
		super(fenetre);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment sauvegarder la partie ?") == JOptionPane.YES_OPTION) {
			this.fenetre.sauvegarderVueMastermindFichier("sauvegarde.mastermind");
		}
	}

}
