import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class ControleurMastermind extends ControleurVue {
	private ModeleMastermind modele;

	public ControleurMastermind(VueMastermind vueMastermind) {
		super(vueMastermind);
		this.modele = new ModeleMastermind(this.vueMastermind.getNbCouleurs(), this.vueMastermind.getTaille());
	}

	public void initialiser() {
		this.vueMastermind.activerCombinaison(this.modele.getCurrentCombinaison());
		this.modele.genererCombinaison();
		this.vueMastermind.afficherCombinaisonOrdinateur(this.modele.getCombinaison());
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() instanceof JColorButton) {
			JColorButton btn = (JColorButton) e.getSource();
			this.modele.setSelectedColor(btn.getColor());
		} else if(e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();

			if(btn.getText().isEmpty()) {
				if(!this.vueMastermind.appartientCombinaison(btn, this.modele.getCurrentCombinaison())) return;

				if(this.modele.getSelectedColor() != null) {
					btn.setBackground(this.modele.getSelectedColor());
				}
			} else { // Bouton Valider
				int currentCombinaison = this.modele.getCurrentCombinaison();
				if(!this.vueMastermind.combinaisonComplete(currentCombinaison)) return;
				int[] combinaison = this.vueMastermind.combinaisonEnEntiers(currentCombinaison);
				int bp = this.modele.nbChiffresBienPlaces(combinaison);
				int mp = this.modele.nbChiffresMalPlaces(combinaison);
				this.vueMastermind.afficherBP(currentCombinaison, bp);
				this.vueMastermind.afficherMP(currentCombinaison, mp);
				// TODO: Check nb essais
				if(this.vueMastermind.combinaisonComplete(currentCombinaison)) {
					if(bp != this.vueMastermind.getTaille() && currentCombinaison + 1 < VueMastermind.NBMAX_COMBINAISONS) {
						this.vueMastermind.activerCombinaison(currentCombinaison + 1);
						this.modele.nextCombinaison();
					} else {
						this.vueMastermind.afficherCombinaisonOrdinateur(this.modele.getCombinaison());
					}
				}
			}
		}
	}


}
