import java.awt.event.ActionEvent;

public class ControleurQuitter extends ControleurFenetre {

	public ControleurQuitter(FenetreMastermind fenetre) {
		super(fenetre);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.exit(0);
	}

}
