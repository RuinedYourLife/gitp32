import java.awt.event.MouseAdapter;

public abstract class ControleurVue extends MouseAdapter {
	protected VueMastermind vueMastermind;
	
	public ControleurVue(VueMastermind vueMastermind) {
		this.vueMastermind = vueMastermind;
	}
}
