import java.awt.event.ActionEvent;

public class ControleurRejouer extends ControleurFenetre {

	public ControleurRejouer(FenetreMastermind fenetre) {
		super(fenetre);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.fenetre.creerNouvelleVueMastermind();
	}
}
